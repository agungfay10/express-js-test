const path = require("path");
const fs = require("fs");
const Product = require("./model");

const create = async (req, res) => {
  const { user_id, name, price, stock, status } = req.body;
  const image = req.file;
  if (image) {
    const target = path.join(__dirname, "../../uploads", image.originalname);
    fs.renameSync(image.path, target);
  }
  try {
    await Product.sync();
    const result = await Product.create({
      user_id,
      name,
      price,
      stock,
      status,
      image_url: `http://localhost:3000/public/${image.originalname}`,
    });
    res.send(result);
  } catch (e) {
    res.send(e);
  }
};

const read = async (req, res) => {
  try {
    await Product.sync();
    const result = await Product.findAll();
    res.send(result);
  } catch (e) {
    res.send(e);
  }
};

const update = async (req, res) => {
  const { user_id, name, price, stock, status } = req.body;
  const image = req.file;
  if (image) {
    const target = path.join(__dirname, "../../uploads", image.originalname);
    fs.renameSync(image.path, target);
  }
  try {
    const id = req.params.id;
    await Product.sync();
    const result = {
      user_id,
      name,
      price,
      stock,
      status,
      image_url: `http://localhost:3000/public/${image.originalname}`,
    };
    await Product.update(result, {
      where: { id },
    });
    res.send(result);
  } catch (e) {
    res.send(e);
  }
};

const destroy = async (req, res) => {
  try {
    const id = req.params.id;
    await Product.sync();
    await Product.destroy({
      where: { id },
    });
    res.send({
      status: "Berhasil Menghapus",
    });
  } catch (e) {
    res.send(e);
  }
};

module.exports = {
  create,
  read,
  update,
  destroy,
};
