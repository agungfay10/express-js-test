const router = require("express").Router();
const multer = require("multer");
const upload = multer({ dest: "uploads" });
const productController = require("./controller");

router.post("/product", upload.single("image_url"), productController.create);
router.get("/product", productController.read);
router.put("/product/:id", upload.single("image_url"), productController.update);
router.delete("/product/:id", upload.single("image_url"), productController.destroy);

module.exports = router;
